// Copyright (c) 2018 ml5
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

/* ===
ml5 Example
Style Transfer Mirror Example using p5.js
This uses a pre-trained model of The Great Wave off Kanagawa and Udnie (Young American Girl, The Dance)
=== */

let style;
let video;
let isTransferring = false;
let resultImg;

// possible values:
// udnie, scream, mathura, fuchun, la_muse, rain_princess, wave, wreck, zhangdaqian
// change the string to any of the possible values, save the file and refresh the page in the browser (CMD + r)
let MODEL = 'scream'; 

function setup() {
  createCanvas(windowWidth, windowHeight).parent('canvasContainer');

  video = createCapture(VIDEO);
  video.hide();
  frameRate(10);
  console.log(MODEL);

  // The results image from the style transfer
  resultImg = createImg('');
  resultImg.hide();

  // The button to start and stop the transfer process
  select('#startStop').mousePressed(startStop);

  // Create a new Style Transfer method with a defined style.
  // We give the video as the second argument
  style = ml5.styleTransfer('../assets/models/' + MODEL, video, modelLoaded);
  // set original image
  select('#original-image').attribute('src', '../assets/images/' + MODEL + '.jpg');

}

function draw(){
  // Switch between showing the raw camera or the style
  if (isTransferring) {
    image(resultImg, 0, 0, windowWidth, windowWidth * video.height / video.width);
  } else {
    image(video, 0, 0, windowWidth, windowWidth * video.height / video.width);
  }
}

// A function to call when the model has been loaded.
function modelLoaded() {
  console.log('Model loaded.')
  select('#startStop').show()
}

// Start and stop the transfer process
function startStop() {
  if (isTransferring) {
    select('#startStop').html('Start');
  } else {
    select('#startStop').html('Stop');
    // Make a transfer using the video
    style.transfer(gotResult);
  }
  isTransferring = !isTransferring;
}

// When we get the results, update the result image src
function gotResult(err, img) {
  if (err) {
    console.log(err);
  } else {
    resultImg.attribute('src', img.src);
  }
  if (isTransferring) {
    style.transfer(gotResult);
  }
}
