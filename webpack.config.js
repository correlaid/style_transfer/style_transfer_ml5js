// This library allows us to combine paths easily
const path = require('path');module.exports = {
    entry: path.resolve(__dirname, 'js/sketch.js'),
    output: {
       path: path.resolve(__dirname, 'output'),
       filename: 'bundle.js'
    },
    resolve: {
       extensions: ['.js', '.jsx']
    },
    devServer: {
        contentBase: './',
        publicPath: '/output'
     }
 };

