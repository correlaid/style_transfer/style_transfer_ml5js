# style_transfer_ml5js_video

Demo for style transfer.
Models from https://github.com/yining1023/fast_style_transfer_in_ML5/ . 

## install
`yarn install`

## dev server
`yarn run dev`

i followed this guide to set up the dev server (yarn run dev):
https://medium.com/front-end-weekly/what-are-npm-yarn-babel-and-webpack-and-how-to-properly-use-them-d835a758f987

-> open `localhost:8080` in chrome. 

### how to: change model
1. open `sketch.js` in the editor (should be open already). 
2. change the variable `MODEL` to any of the possible values (listed there).
3. Save the file (CMD + s).
4. Go to google chrome and refresh the page (CMD + r).

## build
